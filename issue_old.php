<?php
include './php/dbconnect.php';
function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
session_start();
if(!isset($_SESSION['admin']))
{
  header('Location:components.php');
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Tinkerers' lab">
    <meta name="author" content="">

    <title>Projects@TL</title>

    <!-- Bootstrap core CSS -->
  <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href="css/bootstrap.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Fascinate+Inline|Covered+By+Your+Grace' rel='stylesheet' type='text/css'>
    <link href="css/one-page-wonder.css" rel="stylesheet">
    <link rel="shortcut icon" href="http://stab-iitb.org/tinkerers-lab/icon.ico">
     <style type="text/css">
      .panel-title{
        overflow:auto;
      }
      .panel-title a{
        display: block;
        font-weight: bold;
        font-size: 1.2em;
        float: left;
        text-transform: uppercase;
      }

    </style>
  </head>

  <body>

    <nav class="navbar navbar-fixed-top navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">Tinkerers' Lab</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
            <li ><a href="./">About</a></li>
            <!-- <li><a href="./rules.php">Rules</a></li> --><li>

  <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
    Components <span class="caret"></span>
  </a>


  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
     <li><a href="./components.php">New TL</a></li>
            <li><a href="./components_old.php">Old TL</a></li>
  </ul>

</li> 
<li class="active"><a href="./issue_old.php">Issue</a></li>
            <li><a href="./returned_old.php">Return</a></li>
            <li><a href="./projects.php">Projects</a></li>
            <li><a href="./contact.php">Contact</a></li>
            
            
            <li><a href="./logout_old.php">Logout</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
     <div class="col-lg-8 col-lg-offset-4" style="position:relative;top:20px;font-size:35px;font-weight:bold">Welcome to OLD TL issue portal</div>
     <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="page-header">
            <h1 class="heading">
              <span style="font-family:Arial;font-size:50px;font-weight:bold">Issue Components</span>
            </h1>
          </div>
         
          <div class="panel-group" id="accordion">
          <?php
            $q=mysqli_query($con,"select * from types");
            while($row=mysqli_fetch_assoc($q)){
              echo  '<div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#'.clean($row['name']).'">
                          '.$row['name'].'
                        </a>
                      </h4>
                    </div>
                    <div id="'.clean($row['name']).'" class="panel-collapse collapse">
                      <div class="panel-body">
                        ';
                      echo '<table class="table">
                      <tr>
                      <th>Sl no</th>
                      <th>'.$row['fieldname'].'</th>
                      <th>Quantity</th>
                      <th>Items Avialable</th>
                      <th>Issue</th>
                      <th>Quantity to be issued</th>
                      </tr>';

                        $q2=mysqli_query($con,'select * from items where type="'.mysqli_real_escape_string($con,$row['name']).'" and tl = 2');
                        $i=0;
                        while($item=mysqli_fetch_assoc($q2)){
                          $i++;
                          echo '<tr><form id="trying" ><td>'.$i.'</td><td>'.$item['name'].'</td><td>'.$item['quantity'].'</td><td>'.($item['quantity']-$item['issued']).'</td>
                                <td><input type="checkbox" name="issued" value="'.$item['id'].'"></td><td><input type="hidden" id="name'.clean($item['id']).'" value="'.$row['name'].",".$item['name'].'"><input type="text" id="quantity'.clean($item['id']).'" placeholder="No.of items to be issued"></td></form></tr>';
                        }
                      echo '</table>';
                      echo '</div>

                    </div>

                  </div>';

            }
             //echo' <button type="submit" class="btn btn-default" id="issued" onclick="issued()" style="position:relative;left:300px;margin:50px;font-family:Arial;font-size:15px;font-weight:bold" >Done</button>';

          ?>
 


              
              <br><br><br><br>
			<form role="form" id="issue">
			  <div class="form-group">
			    <label for="exampleInputEmail1" style="font-family:Arial;font-size:25px;font-weight:bold">Name</label>
			    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name"  style="font-family:Arial;font-size:15px;" required>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1"  style="font-family:Arial;font-size:25px;font-weight:bold">Roll no</label>
			    <input type="number" class="form-control" name="roll" placeholder="Enter your Roll no."  style="font-family:Arial;font-size:15px;" required>
			  </div>
			   <div class="form-group">
			    <label for="exampleInputPassword1"  style="font-family:Arial;font-size:25px;font-weight:bold">Contact no.</label>
			    <input type="number" class="form-control" name="phone" placeholder="Enter your Mobile no."  style="font-family:Arial;font-size:15px;" required>
			  </div>
			   <input type="hidden" name="issue" id="components">
			   <button type="submit" class="btn btn-default"  style="font-family:Arial;font-size:15px;font-weight:bold"  >Submit</button>
			</form>
            </h1>
          </div>



        </div>
      </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>

        <!-- /END THE FEATURETTES -->
      <hr>

      <!-- FOOTER -->
      <footer style="text-align:center">
        <p>Copyright &copy; STAB 2013-14 IITB</p>
        <a href="http://stab-iitb.org/">&middot; STAB IITB </a> <br/>
        <a href="http://techid.stab-iitb.org">&middot; Techid STAB </a>
      </footer>
      <!-- /END OF FOOTER -->

    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">LOGIN</h4>
          </div>
          <div class="modal-body">
            <BLOCKQUOTE>Login in tikerers Lab is only allowed to managers. Please visit tinkerer's lab to issue components</BLOCKQUOTE>
            <form id="admin-login">
            <div class="form-group">
              <label class="col-md-5"> Admin Password:
              </label>
              <div class="col-md-7">
                <input type="password" name="admin-password" class="form-control"> 
              </div>
            </div>
          </div>
          <br>
          <br>
          <br>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button class="btn btn-primary">Login </button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
    <script>
    
      $('#issue').submit(function(e){
      	e.preventDefault();
      	 a=$('input[type=checkbox]:checked');
        data={};
        for(var i=0;i<a.length;i++)
		{
			data[a[i].value]=$('#quantity'+a[i].value).val();

			if($('#quantity'+a[i].value).val()==''){
				alert('Please write quantity to be issued for '+$('#name'+a[i].value).val());
				return;
			}
		}
		if(a.length==0){
			alert('Please select atleast one object to issue');
			return;
		}
		$("#components").val(JSON.stringify(data));

		 jQuery.ajax({
          url:'php/issue_old.php',
          data:$(this).serialize(),     
          type:'post',
          success:function(data){
            if(data=="done"){
            location.reload();
            alert('Issued Successfully');
          }

            else 
              alert('Unable to issue : maybe you are trying to issue more than available');
            
            console.log(data);
        },
          
          error:function(){
            alert('Error in adding data. No Response from server');
          }
		})
      })
    </script>
  
  </body>



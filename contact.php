<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Tinkerers' lab">
    <meta name="author" content="">

    <title>Tinkerers' Lab</title>

    <!-- Bootstrap core CSS -->
  <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href="css/bootstrap.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Fascinate+Inline|Covered+By+Your+Grace' rel='stylesheet' type='text/css'>
    <link href="css/one-page-wonder.css" rel="stylesheet">
    <link rel="shortcut icon" href="http://stab-iitb.org/tinkerers-lab/icon.ico">
  </head>

  <body>

    <nav class="navbar navbar-fixed-top navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">Tinkerers' Lab</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
            <li ><a href="./">About</a></li>
            <!-- <li><a href="./rules.php">Rules</a></li> --><li><a href="./components.php">Components New TL</a></li> 
            <li><a href="./components_old.php">Components Old TL</a></li> 
            <li ><a href="./projects.php">Projects</a></li>
            <li class="active"><a href="./contact.php">Contact</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="page-header">
            <h1 class="heading">
              Contact Us
            </h1>
          </div>
    
          
        <table border="0" align="center">
<tbody>
<tr>
<td colspan="3">
<h2>Overall Co-ordinator:</h2>
</td>
</tr>
<tr>
<td>
<h2><span style="font-size: 14pt;">Rahul Prajapat</span></h2>
</td>
<td>
<h2><span style="font-size: 12pt;"><a href="mailto:rahul.prajapat9@gmail.com">rahul.prajapat9@gmail.com</a></span></h2>
</td>
<td>
<h2> <span style="font-size: 12pt;">9892945399</span></h2>
</td>
</tr>
<tr>
<td colspan="3">
<h2>Club Managers:</h2>
</td>
</tr>
<tr>
<td colspan="3"><span style="font-size: 12pt;"><b>Aeromodelling Club:</b></span></td>
</tr>
<tr>
<td>Kuldeep</td>
<td><a href="mailto:kuldeepsingh050895@gmail.com">kuldeepsingh050895@gmail.com</a></td>
<td>9920136821</td>
</tr>
<tr>
<td colspan="3"><span style="font-size: 12pt;"><b>Electronics Club:</b></span></td>
</tr>
<tr>
<td>Riddhish</td>
<td><a href="mailto:riddhishb@gmail.com">riddhishb@gmail.com</a></td>
<td>9769472731</td>
</tr>
<tr>
<td>Ankita</td>
<td><a dir="ltr" href="mailto:aesha.ajp@gmail.com" target="_blank" rel="nofollow">aesha.ajp@gmail.com</a></td>
<td> 9773619340</td>
</tr>
<tr>
<td colspan="3"><span style="font-size: 12pt;"><b>Krittika:</b></span></td>
</tr>
<tr>
<td>Sharad</td>
<td><a href="mailto:sharad157@gmail.com">sharad157@gmail.com</a></td>
<td>9619589656</td>
</tr>
<tr>
<td colspan="3"><span style="font-size: 12pt;"><b>Maths &amp; Physics Club:</b></span></td>
</tr>
<tr>
<td>Anchal</td>
<td><a href="mailto:anchal.physics@gmail.com">anchal.physics@gmail.com</a></td>
<td>8286005181</td>
</tr>
<tr>
<td colspan="3"><span style="font-size: 12pt;"><b>Robotics Club:</b></span></td>
</tr>
<tr>
<td>Chetan</td>
<td><a href="mailto:chetan.agrawal39@gmail.com">chetan.agrawal39@gmail.com</a></td>
<td>9167469004</td>
</tr>
<tr>
<td>Vineetha</td>
<td><a href="mailto:vinni.iitb@gmail.com">vinni.iitb@gmail.com</a></td>
<td>9819915441</td>
</tr>
<tr>
<td colspan="3"><span style="font-size: 12pt;"><b>Web and Coding Club:</b></span></td>
</tr>
<tr>
<td>Ranveer Aggarwal</td>
<td><a href="mailto:ranveeraggarwal@gmail.com">ranveeraggarwal@gmail.com</a></td>
<td>7588557512</td>
</tr>
<tr>
<td>Manish Goregaokar</td>
<td><a href="mailto:manishsmail@gmail.com">manishsmail@gmail.com</a></td>
<td>9167337978</td>
</tr>
<tr>
<td colspan="3">
<h3><strong><span style="font-size: 12pt;">Technovation:</span></strong></h3>
</td>
</tr>
<tr>
<td>Shyam Sunder</td>
<td><a href="mailto:ssprasad49@gmail.com">ssprasad49@gmail.com</a></td>
<td>7738450101</td>
</tr>
<tr>
<td>Manmohan</td>
<td><a href="mailto:manmohanmandhana000@gmail.com">manmohanmandhana000@gmail.com</a></td>
<td>9769486341</td>
</tr>
<tr>
<td>Dewashish Kumar Dey</td>
<td><a href="mailto:dewashish.dey05@gmail.com">dewashish.dey05@gmail.com</a></td>
<td>7738258332</td>
</tr>
<tr>
<td colspan="3">
<h3><span style="font-size: 12pt;"><strong>Web Manager:</strong></span></h3>
</td>
</tr>
<tr>
<td>Prateek Chandan</td>
<td><a href="mailto:prateekchandan5545@gmail.com">prateekchandan5545@gmail.com</a></td>
<td>9619730024</td>
</tr>
<tr>
<td colspan="3">
<h3><span style="font-size: 12pt;"><strong>Publi and Media Manager:</strong></span></h3>
</td>
</tr>
<tr>
<td>Sagun Pai</td>
<td><a href="#">--</a></td>
<td>--</td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</tbody>
</table> 
        

        <!-- /END THE FEATURETTES -->
      <hr>

      <!-- FOOTER -->
      <footer style="text-align:center">
        <p>Copyright &copy; STAB 2013-14 IITB</p>
        <a href="http://stab-iitb.org/">&middot; STAB IITB </a> <br/>
        <a href="http://techid.stab-iitb.org">&middot; Techid STAB </a>
      </footer>
      <!-- /END OF FOOTER -->

    </div>


    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>

  
  </body>

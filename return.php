<?php
include './php/dbconnect.php';
function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
session_start();
if(!isset($_SESSION['admin']))
{
  header('Location:returned.php');
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Tinkerers' lab">
    <meta name="author" content="">

    <title>Projects@TL</title>

    <!-- Bootstrap core CSS -->
  <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href="css/bootstrap.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Fascinate+Inline|Covered+By+Your+Grace' rel='stylesheet' type='text/css'>
    <link href="css/one-page-wonder.css" rel="stylesheet">
    <link rel="shortcut icon" href="http://stab-iitb.org/tinkerers-lab/icon.ico">
     <style type="text/css">
      .panel-title{
        overflow:auto;
      }
      .panel-title a{
        display: block;
        font-weight: bold;
        font-size: 1.2em;
        float: left;
        text-transform: uppercase;
      }

    </style>
  </head>

  <body>

    <nav class="navbar navbar-fixed-top navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">Tinkerers' Lab</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
            <li ><a href="./">About</a></li>
            <!-- <li><a href="./rules.php">Rules</a></li> --><li class="active"><a href="./components.php">Components New TL</a></li> 
            <li><a href="./projects.php">Projects</a></li>
            <li><a href="./contact.php">Contact</a></li>
            <li><a href="./issue.php">Issue</a></li>
            <li><a href="./returned.php">Return</a></li>
            <li><a href="./logout.php">Logout New TL Admin</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
     <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="page-header">
            <h1 class="heading">
              
            </h1>
          </div>
         
 



			<form class="form-inline" role="form" id="return">
  <div class="form-group">
    <label class="sr-only" for="exampleInputEmail2">Name</label>
    <input type="name" class="form-control" id="exampleInputEmail2" name="name" placeholder="Enter name">
  </div>
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword2">Roll no.</label>
    <input type="text" class="form-control" id="exampleInputPassword2" name="roll" placeholder="Enter Roll no.">
  </div>
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword2">Contact No.</label>
    <input type="number" class="form-control" id="exampleInputPassword2" name="phone" placeholder="Mobile no.">
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
            </h1>
          </div>



        </div>
      </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>

        <!-- /END THE FEATURETTES -->
      <hr>

      <!-- FOOTER -->
      <footer style="text-align:center">
        <p>Copyright &copy; STAB 2013-14 IITB</p>
        <a href="http://stab-iitb.org/">&middot; STAB IITB </a> <br/>
        <a href="http://techid.stab-iitb.org">&middot; Techid STAB </a>
      </footer>
      <!-- /END OF FOOTER -->

    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">LOGIN</h4>
          </div>
          <div class="modal-body">
            <BLOCKQUOTE>Login in tikerers Lab is only allowed to managers. Please visit tinkerer's lab to issue components</BLOCKQUOTE>
            <form id="admin-login">
            <div class="form-group">
              <label class="col-md-5"> Admin Password:
              </label>
              <div class="col-md-7">
                <input type="password" name="admin-password" class="form-control"> 
              </div>
            </div>
          </div>
          <br>
          <br>
          <br>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button class="btn btn-primary">Login </button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
    <script>

$("#return").submit(function(e){
      e.preventDefault();
      jQuery.ajax({
          url:'php/check.php',
          data:$(this).serialize(),
          type:'post',
          success:function(data){
            if(data=="done")
            location.reload();

            else if(data=='password-error'){
              alert('Check Your Credentials');
            }
            else 
              alert('Some error occured while login');

            console.log(data);
          },
          error:function(){
            alert('Error in login. No Response from server');
          }
        });
    })
     /* $('#issue').submit(function(e){
      	e.preventDefault();
      	 a=$('input[type=checkbox]:checked');
        data={};
        for(var i=0;i<a.length;i++)
		{
			data[a[i].value]=$('#quantity'+a[i].value).val();
			if($('#quantity'+a[i].value).val()==''){
				alert('Please write quantity to be issued for '+$('#name'+a[i].value).val());
				return;
			}
		}
		if(a.length==0){
			alert('Please select atleast one object to issue');
			return;
		}
		$("#components").val(JSON.stringify(data));

		 jQuery.ajax({
          url:'php/issue.php',
          data:$(this).serialize(),
          type:'post',
          success:function(data){
            if(data=="done")
            location.reload();

            else 
              alert('Unable to issue');

            console.log(data);
        },
          
          error:function(){
            alert('Error in adding data. No Response from server');
          }
		})
      })*/
    </script>
  
  </body>



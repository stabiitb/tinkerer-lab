<?php
include './php/dbconnect.php';
function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
session_start();
if(!isset($_SESSION['admin']))
{
  header('Location:components.php');
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Tinkerers' lab">
    <meta name="author" content="">

    <title>Projects@TL</title>

    <!-- Bootstrap core CSS -->
  <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href="css/bootstrap.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Fascinate+Inline|Covered+By+Your+Grace' rel='stylesheet' type='text/css'>
    <link href="css/one-page-wonder.css" rel="stylesheet">
    <link rel="shortcut icon" href="http://stab-iitb.org/tinkerers-lab/icon.ico">


    <style type="text/css">
      .panel-title{
        overflow:auto;
      }
      .panel-title a{
        display: block;
        font-weight: bold;
        font-size: 1.2em;
        float: left;
        text-transform: uppercase;
      }

    </style>
  </head>

  <body>

    <nav class="navbar navbar-fixed-top navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">Tinkerers' Lab</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
            <li ><a href="./">About</a></li>
            <!-- <li><a href="./rules.php">Rules</a></li> --> <li>

  <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
    Components <span class="caret"></span>
  </a>


  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
     <li><a href="./components.php">New TL</a></li>
            <li><a href="./components_old.php">Old TL</a></li>
  </ul>

</li>
       
            <li><a href="./issue.php">Issue</a></li>
            <li><a href="./returned.php">Return</a></li>
            <li><a href="./projects.php">Projects</a></li>
            <li><a href="./contact.php">Contact</a></li>
             <li><a href="./logout.php">Logout</a></li>
           </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
    <div class="col-lg-5 col-lg-offset-5" style="position:relative;top:20px;font-size:40px;font-weight:bold">Welcome to NEW TL</div>
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-lg-offset-2">
          <div class="page-header">
            <h1 class="heading">
              Components
            </h1>
          </div>
         
          <div class="panel-group" id="accordion">
          <?php
            $q=mysqli_query($con,"select * from types ");
            while($row=mysqli_fetch_assoc($q)){
              echo  '<div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#'.clean($row['name']).'">
                          '.$row['name'].'
                        </a>
                        <button class=" pull-right btn btn-danger" onclick="deletetype(\''.$row['name'].'\')" >Delete</button>
                      </h4>
                    </div>
                    <div id="'.clean($row['name']).'" class="panel-collapse collapse">

                      <div class="panel-body">
                        ';
                      echo '<table class="table">
                      <tr>
                      <th>Sl no</th>
                      <th>'.$row['fieldname'].'</th>
                      <th>Quantity</th>
                      <th>Items Avialable</th>
                      <th>UPDATE/DELETE</th>
                      </tr>';

                        $q2=mysqli_query($con,'select * from items where type="'.mysqli_real_escape_string($con,$row['name']).'" and tl = 1');
                        $i=0;
                        while($item=mysqli_fetch_assoc($q2)){
                          $i++;
                          echo '<tr><form id="item'.$item['id'].'"><td>'.$i.'</td><td>'.$item['name'].'</td><td><input name="id" value="'.$item['id'].'" type="hidden"><input name="quantity" style="width:50%" type="number" value="'.$item['quantity'].'"</td><td>'.($item['quantity']-$item['issued']).'</td><td></form>
                          <button class="btn btn-success"style="margin-right:10px;" onclick="updateitems(\''.$item['id'  ].'\')">Update </button><button class="btn  btn-danger" onclick="deleteitems(\''.$item['id'  ].'\')">Delete</button></td></tr>';
                        }

                        echo '<tr><form id="item'.clean($row['name']).'"><td></td><td><input type="hidden" name="type" value="'.$row['name'].'"><input name="name" placeholder="Name"></td><td><input name="quantity" type="number" placeholder="quantity of item"></td><td></td><td></form>
                          <button class="btn btn-success"style="margin-right:10px;float:right" onclick="additems(\'item'.clean($row['name']).'\')">Add new item</button></td></tr>';
                     
                      echo '</table>';
                      echo'   <div class="panel panel-default">
                  
                  </div>';
                      echo '</div>
                    </div>
                  </div>';
            }


          ?>
          <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        
                        <div data-toggle="collapse" data-parent="#accordion" href="#addnewtype" class="btn pull-right btn-success" >Click  here  to  add  a  new  type</div>
                      </h4>
                    </div>
                    <div id="addnewtype" class="panel-collapse collapse">

                      <div class="panel-body">
                        <form role="form" id="addnewtype-form">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter type eg:capacitor" name="name"  required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Fieldname</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Fieldname eg: OHM , NAME" name="fieldname" required>
                          </div>
                          <button type="submit" class="btn btn-default" >Submit</button>
                        </form>
                      </div>
                    </div>
                  </div>
 

</div>



        </div>
      </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>

        <!-- /END THE FEATURETTES -->
      <hr>

      <!-- FOOTER -->
      <footer style="text-align:center">
        <p>Copyright &copy; STAB 2013-14 IITB</p>
        <a href="http://stab-iitb.org/">&middot; STAB IITB </a> <br/>
        <a href="http://techid.stab-iitb.org">&middot; Techid STAB </a>
      </footer>
      <!-- /END OF FOOTER -->

    </div>


    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript">
    function deletetype (name) {
      var chk=confirm('Are you sure want to delete all '+name+'? '+name+' will be deleted from both new and old tl ');
      if(chk){
        jQuery.ajax({
          url:'php/deletetype.php',
          data:{name:name},
          type:'post',
          success:function(data){
            if(data=="done")
            location.reload();

            else 
              alert('Unable to delete component');

            console.log(data);
          },
          error:function(){
            alert('Error in deleting data. No Response from server');
          }
        });
      }
      // body...
    }
     function updateitems (id) {
      
        var data = $("#item"+id).serialize();
        
        jQuery.ajax({
          url:'php/updateitems.php',
          data:data,
          type:'post',
          success:function(data){
            if(data=="done")
            location.reload();

            else 
              alert('Unable to update component');

            console.log(data);},
          
          error:function(){
            alert('Error in updating data. No Response from server');
          }
        });
      }
      // body...

       function deleteitems (id) {
             var data = $("#item"+id).serialize();  
             var chk=confirm('Are you sure want to delete ?');
             if(chk){
              jQuery.ajax({
                url:'php/deleteitems.php',
                data:data,
                type:'post',
                success:function(data){
                  if(data=="done")
                  location.reload();

                  else 
                    alert('Unable to delete component');

                  console.log(data);
                },
                error:function(){
                  alert('Error in deleting data. No Response from server');
                }
              });
            
            // body...
          }
        }

    
      $("#addnewtype-form").submit(function(e){
        e.preventDefault();
        var data=$(this).serialize();

        jQuery.ajax({
          url:'php/addtype.php',
          data:data,
          type:'post',
          success:function(data){
            if(data=="done")
            location.reload();

            else 
              alert('Unable to add type');

            console.log(data);},
          
          error:function(){
            alert('Error in adding data. No Response from server');
          }
        });
      })

      function additems(id){
        var data=$('#'+id).serialize();
        jQuery.ajax({
          url:'php/additem.php',
          data:data,
          type:'post',
          success:function(data){
            if(data=="done")
            location.reload();

            else 
              alert('Unable to add type');

            console.log(data);},
          
          error:function(){
            alert('Error in adding data. No Response from server');
          }
        });
      }
    </script>
    <script>

    </script>
  
  </body>
-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 06, 2014 at 10:21 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `technovation`
--
CREATE DATABASE IF NOT EXISTS `technovation` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `technovation`;

-- --------------------------------------------------------

--
-- Table structure for table `issued`
--

CREATE TABLE IF NOT EXISTS `issued` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `roll` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `list` varchar(10000) NOT NULL DEFAULT '{}',
  `quantityissued` int(100) NOT NULL,
  `tl` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

--
-- Dumping data for table `issued`
--

INSERT INTO `issued` (`id`, `name`, `roll`, `phone`, `date`, `list`, `quantityissued`, `tl`) VALUES
(34, 'ak', '123', '123', '2014-06-01 19:47:39', 'Rfmodule', 10, 0),
(40, 'sd', '53', '19', '2014-06-01 19:14:49', 'Arduino', 5, 0),
(41, 'sd', '53', '19', '2014-06-01 19:19:27', 'Arduino', 5, 0),
(42, 'sdf', '213', '12', '2014-06-01 19:22:53', 'Arduino', 1, 0),
(43, 'daa', '53', '20', '2014-06-01 19:26:37', 'Arduino', 1, 0),
(74, 'hj', '32', '23', '2014-06-06 09:41:07', '21', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) NOT NULL,
  `type` varchar(500) NOT NULL,
  `quantity` int(11) NOT NULL,
  `issued` int(11) NOT NULL DEFAULT '0',
  `tl` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `type`, `quantity`, `issued`, `tl`) VALUES
(10, 'ds', 'RESISTORS', 243, 45, 0),
(12, 'Arduino', 'Development board', 10, 10, 0),
(16, 'Rfmodule', 'Development board', 50, 0, 0),
(17, '2', 'Capacitor', 15, 0, 1),
(19, '3', 'Capacitor', 21, 21, 2),
(20, 'Arduino Board', 'Development board', 12, 12, 2),
(21, 'Arduino_Board', 'Development board', 13, 2, 2),
(22, '4', 'Capacitor', 20, 20, 2);

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `name` varchar(500) NOT NULL,
  `fieldname` varchar(500) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`name`, `fieldname`) VALUES
('Capacitor', 'FARAD'),
('Development board', 'name'),
('printers', 'NAME'),
('RESISTORS', 'OHMS');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

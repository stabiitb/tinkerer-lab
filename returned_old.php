<?php
include './php/dbconnect.php';

function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
session_start();
if(!isset($_SESSION['admin']))
{
  header('Location:return.php');
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Tinkerers' lab">
    <meta name="author" content="">

    <title>Projects@TL</title>

    <!-- Bootstrap core CSS -->
  <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href="css/bootstrap.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Fascinate+Inline|Covered+By+Your+Grace' rel='stylesheet' type='text/css'>
    <link href="css/one-page-wonder.css" rel="stylesheet">
    <link rel="shortcut icon" href="http://stab-iitb.org/tinkerers-lab/icon.ico">


    <style type="text/css">
      .panel-title{
        overflow:auto;
      }
      .panel-title a{
        display: block;
        font-weight: bold;
        font-size: 1.2em;
        float: left;
        text-transform: uppercase;
      }

    </style>
  </head>

  <body>

    <nav class="navbar navbar-fixed-top navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">Tinkerers' Lab</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
            <li ><a href="./">About</a></li>
            <!-- <li><a href="./rules.php">Rules</a></li> --><li>

  <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
    Components <span class="caret"></span>
  </a>


  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
     <li><a href="./components.php">New TL</a></li>
            <li><a href="./components_old.php">Old TL</a></li>
  </ul>

</li>
<li><a href="./issue.php">Issue</a></li>
              <li class="active"><a href="./returned.php">Return</a></li>
            <li><a href="./projects.php">Projects</a></li>
            <li><a href="./contact.php">Contact</a></li>
             
             <li><a href="./logout_old.php">Logout </a></li>
           </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
     <div class="col-lg-8 col-lg-offset-4" style="position:relative;top:20px;font-size:35px;font-weight:bold">Welcome to OLD TL return portal</div>
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-lg-offset-0">
          <div class="page-header">
            <h1 class="heading">
             <span style="font-family:Arial;font-size:50px;font-weight:bold">Return Components</span>
            </h1>
          </div>
         
          <div class="panel-group" id="accordion">
          <?php
            $q=mysqli_query($con,"select distinct roll from issued where tl = 2 ");
            while($row=mysqli_fetch_assoc($q)){
              echo  '<div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#'.clean($row['roll']).'">
                          '.$row['roll'].'
                        </a>
                        <button class=" pull-right btn btn-primary" onclick="submitallcomp(\''.$row['roll'].'\')" >Submit All</button>
                      </h4>
                    </div>
                    <div id="'.clean($row['roll']).'" class="panel-collapse collapse">

                      <div class="panel-body">
                        ';
                      echo '<table class="table">
                      <tr>
                      <th>Sl no</th>
                      <th>Name</th>
                      <th>Roll No.</th>
                      <th>Phone no.</th>
                      <th>Date/Time</th>
                      <th>Component</th>
                      <th>Quantity</th>
                      <th>Select</th>
                      <th>Quantity returned</th>
                      </tr>';
                        $q2=mysqli_query($con,'select * from issued where roll="'.mysqli_real_escape_string($con,$row['roll']).'" and tl = 2');
                        $i=0;
                        while($item=mysqli_fetch_assoc($q2)){
                          $show="";
                          $q3=mysqli_query($con,'select * from items where tl = 2');
                          while($itemcheck=mysqli_fetch_assoc($q3)){
                            
                            if($itemcheck['id']==$item['list'])
                            {
                            $show=$itemcheck['type']."-".$itemcheck['name'];
                            break;
                            }
                          }
                          $i++;
                          echo '<tr><form id="item'.$item['id'].'"><td>'.$i.'</td><td>'.$item['name'].'</td><td>'.$item['roll'].'</td><td>'.($item['phone']).'</td>
                          <td>'.($item['date']).'</td><td>'.$show.'</td><td>'.($item['quantityissued']).'</td><td><input type="checkbox" name="issued" 
                          value="'.$item['list'].'"></td><td><input type="hidden" id="name'.clean($item['list']).'" value="'.$item['list'].'"><input type="text" id="quantity'.clean($item['list']).'" placeholder="No.of items returned"></td>
                          </form>
                          </tr>';
                        }
                        
                      echo '</table>';
                      echo'<form role="form" id="issue"><input type="hidden" name="issue" id="components"><input type="hidden" name="name" value="'.$item['name'].'"><button type="submit" class="btn btn-primary"
                       style="position:relative;left:500px;margin-bottom:10px">Submit</button></form>';
                      echo'   <div class="panel panel-default">
                  
                  </div>';
                      echo '</div>
                    </div>
                  </div>';
            }


          ?>
         
 

</div>



        </div>
      </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>

        <!-- /END THE FEATURETTES -->
      <hr>

      <!-- FOOTER -->
      <footer style="text-align:center">
        <p>Copyright &copy; STAB 2013-14 IITB</p>
        <a href="http://stab-iitb.org/">&middot; STAB IITB </a> <br/>
        <a href="http://techid.stab-iitb.org">&middot; Techid STAB </a>
      </footer>
      <!-- /END OF FOOTER -->

    </div>


    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript">
    function submitallcomp (name) {
      var chk=confirm('Are you sure want to submit all components you have issued?');
      if(chk){
        jQuery.ajax({
          url:'php/submitallcomp_old.php',
          data:{name:name},
          type:'post',
          success:function(data){
            if(data=="done")
            location.reload();

            else 
              alert('Unable to submit all component');

            console.log(data);
          },
          error:function(){
            alert('Error in submitting data. No Response from server');
          }
        });
      }
      // body...
    }
     
 $('#issue').submit(function(e){
        e.preventDefault();
         a=$('input[type=checkbox]:checked');
        data={};
        for(var i=0;i<a.length;i++)
    {
      data[a[i].value]=$('#quantity'+a[i].value).val();

      if($('#quantity'+a[i].value).val()==''){
        alert('Please write quantity to be issued for '+$('#name'+a[i].value).val());
        return;
      }
    }
    if(a.length==0){
      alert('Please select atleast one object to return');
      return;
    }
    $("#components").val(JSON.stringify(data));

     jQuery.ajax({
          url:'php/submititems_old.php',
          data:$(this).serialize(),     
          type:'post',
          success:function(data){
            if(data=="done"){
            location.reload();
             alert('Returned Successfully');}

            else 
              alert('Unable to return : maybe you are trying to return more than you have issued');
            
            console.log(data);
        },
          
          error:function(){
            alert('Error in submitting data. No Response from server');
          }
    })
      })
          

    

      
    </script>
    <script>

    </script>
  
  </body>